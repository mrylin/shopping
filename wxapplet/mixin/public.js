export default {
	data() {
		return {
			pnavH: ``,
			connavh: ``,
			pnavT: ``,
			searchindex: 0,
			rich_img: [
				'<img src="https://mrylin.gitee.io/lnsc/produc/info1.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info2.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info3.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info4.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info5.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info6.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info7.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info8.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info9.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info10.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info11.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info12.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info13.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info14.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info15.jpg" />',
				'<img src="https://mrylin.gitee.io/lnsc/produc/info16.jpg" />',
			]
		}
	},
	onLoad() {
		this.pnavH = `${uni.getMenuButtonBoundingClientRect().height}px`;
		this.connavh = uni.getMenuButtonBoundingClientRect().top + uni.getMenuButtonBoundingClientRect().height + 6 + 'px'
		this.pnavT = `${uni.getMenuButtonBoundingClientRect().top}px 0 0 3%`;
	},
	methods: {
		cutsearch() {
			setInterval(() => {
				if (this.searchindex < (this.allsearch.length) - 1) {
					++this.searchindex;
				} else {
					this.searchindex = 0;
				}
			}, 2000)
		},
		//获取城市
		async reqcity(name) {
			let res = await uni.request({
				url: `https://restapi.amap.com/v3/config/district?keywords=${name}&subdistrict=2&key=49bc0d21f7996b4be4c9175e6bf1665b`
			});
			return res[1].data.districts[0].districts;
		},
		mlogin() {
			//登录方法
			 try {
				let stor = wx.getStorageSync('userInfo')
				if (stor == '') {
					wx.getUserProfile({
						desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
						success: (res) => {
							wx.setStorageSync('userInfo', res.userInfo.nickName)
							this.$store.commit('setUser', res.userInfo.nickName)
						}
					})
				}
			} catch (e) {

			}
		},
		logout(){
			this.$store.commit('setUser', '登录')
			wx.clearStorageSync()
		},
		mload() {
			//首次进入判断登录情况
			try {
				let stor = wx.getStorageSync('userInfo')
				if (stor != '') {
					this.$store.commit('setUser', stor)
				}
			} catch (e) {

			}
		}
	},
}
