import Vue from 'vue'

import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		searchdata: [],
		UserInfo:'登录',
	},
	mutations: {
		getsearch(state, data) {
			state.searchdata = data;
		},
		setUser(state, data) {
			state.UserInfo = data;
		}
	},
	action: {
		getsearch(context,data) {
			context.commit("getsearch", data)
		},
		setUser(context,data) {
			context.commit("setUser", data)
		}
	}
})

export default store
