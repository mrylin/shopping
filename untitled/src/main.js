import Vue from 'vue'
import App from './App.vue'
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

import SlideVerify from 'vue-monoplasty-slide-verify';

Vue.use(VueQuillEditor)
Vue.use(SlideVerify);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
