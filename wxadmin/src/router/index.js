import {createRouter, createWebHistory} from 'vue-router'
import admin from '../views/admin'
import index from '../views/index'
import product from '../views/product'
import user from '../views/user'
import category from '../views/category'
import addproduct from '../views/addproduct'
import order from '../views/order'
import login from '../views/login'
import systemuser from '../views/systemuser'
import roles from '../views/roles'
import preview from '../views/preview'

const routes = [
    {
        path: '/',
        name: 'admin',
        component: admin,
        redirect: { //访问根路径的时候重定向到名字为index的组件上
            name: 'index'
        },
        children: [
            {
                path: '/index',
                name: 'index',
                component: index
            },
            {
                path: '/product',
                name: 'product',
                component: product
            },
            {
                path: '/user',
                name: 'user',
                component: user
            },
            {
                path: '/category',
                name: 'category',
                component: category
            },
            {
                path: '/addproduct',
                name: 'addproduct',
                component: addproduct
            },
            {
                path: '/order',
                name: 'order',
                component: order
            },
            {
                path: '/systemuser',
                name: 'systemuser',
                component: systemuser
            },
            {
                path: '/roles',
                name: 'roles',
                component: roles
            },
            {
                path: '/preview',
                name: 'preview',
                component: preview
            },
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: login
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})
//to即将要进入的目标,from当前导航正要离开的路由,next进行管道中的下一个钩子
router.beforeEach((to, from, next) => {
    if (to.name == 'login') {
        next()
    } else {
        let reg = new RegExp("(^| )token=([^;]*)(;|$)");
        let arr = document.cookie.match(reg)
        if (!(arr == null || arr == "" || arr == "null" || typeof (arr) == "undefined" || arr == false || arr == undefined)) {
            if (unescape(arr[2]) === '123456789') {
                next()
            } else {
                next({name: 'login', replace: true})
            }
        } else {
            next({name: 'login', replace: true})
        }
    }
})

export default router
