export default {
    data() {
        return {
            uniqueId: "uniqueId",
            content: ``, // 富文本编辑器默认内容
            editorOption: {
                //  富文本编辑器配置
                modules: {
                    //工具栏定义的
                    toolbar: [
                        ["bold", "italic", "underline", "strike"], // 加粗 斜体 下划线 删除线 -----['bold', 'italic', 'underline', 'strike']
                        ["blockquote", "code-block"], // 引用  代码块-----['blockquote', 'code-block']
                        [{header: 1}, {header: 2}], // 1、2 级标题-----[{ header: 1 }, { header: 2 }]
                        [{list: "ordered"}, {list: "bullet"}], // 有序、无序列表-----[{ list: 'ordered' }, { list: 'bullet' }]
                        [{script: "sub"}, {script: "super"}], // 上标/下标-----[{ script: 'sub' }, { script: 'super' }]
                        [{indent: "-1"}, {indent: "+1"}], // 缩进-----[{ indent: '-1' }, { indent: '+1' }]
                        [{direction: "rtl"}], // 文本方向-----[{'direction': 'rtl'}]
                        [{size: ["small", false, "large", "huge"]}], // 字体大小-----[{ size: ['small', false, 'large', 'huge'] }]
                        [{header: [1, 2, 3, 4, 5, 6, false]}], // 标题-----[{ header: [1, 2, 3, 4, 5, 6, false] }]
                        [{color: []}, {background: []}], // 字体颜色、字体背景颜色-----[{ color: [] }, { background: [] }]
                        [{font: []}], // 字体种类-----[{ font: [] }]
                        [{align: []}], // 对齐方式-----[{ align: [] }]
                        ["clean"], // 清除文本格式-----['clean']
                        ["link", "image", "video"] // 链接、图片、视频-----['link', 'image', 'video']
                    ],
                },
                //主题
                theme: "snow",
                placeholder: "请输入正文"
            },

            //echarts饼图配置
            option: {
                tooltip: { //鼠标移入格式化字
                    trigger: 'item',
                    formatter: '{a} <br/>{b}: {c}' + '位'
                },
                legend: {
                    type: 'scroll',
                    //图例组件展现了不同系列的标记(symbol)，颜色和名字。可以通过点击图例控制哪些系列不显示。
                    orient: 'horizontal',
                    data: ['苹果', '安卓', '平板', '微信PC端'],
                    itemGap: 20,
                    textStyle: { //文字颜色
                        color: "#333"
                    },
                },
                series: [
                    {
                        name: '今日访问人数',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        avoidLabelOverlap: false,
                        emphasis: { //鼠标移入效果
                            label: {
                                show: true,
                                fontWeight: 'bold',
                                color: "#ce2121"
                            }
                        },
                        labelLine: {
                            show: true
                        },
                        data: [
                            {
                                value: 335, name: '苹果',
                                itemStyle: {
                                    color: '#90eae4'
                                }
                            },
                            {
                                value: 310, name: '安卓',
                                itemStyle: {
                                    color: '#0ff52e'
                                }
                            },
                            {
                                value: 234, name: '平板',
                                itemStyle: {
                                    color: '#e7f563'
                                }
                            },
                            {
                                value: 135, name: '微信PC端',
                                itemStyle: {
                                    color: '#a7c5eb'
                                }
                            }
                        ]
                    }
                ]
            },

            rightoption: {
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    type: 'scroll',
                    orient: 'horizontal',
                    data: ['苹果', '安卓', '微信客户端', '平板'],
                    itemGap: 20,
                },
                series: [
                    {
                        name: '购买率',
                        type: 'pie',
                        radius: '70%',
                        emphasis: { //鼠标移入效果
                            label: {
                                show: true,
                                fontWeight: 'bold',
                                color: "#ce2121"
                            }
                        },
                        data: [
                            {
                                name: '苹果',
                                value: 20,
                                itemStyle: {
                                    color: '#90eae4'
                                }
                            },
                            {
                                name: '安卓',
                                value: 100,
                                itemStyle: {
                                    color: '#0ff52e'
                                }
                            },
                            {
                                name: '微信客户端',
                                value: 10,
                                itemStyle: {
                                    color: '#a7c5eb'
                                }
                            },
                            {
                                name: '平板',
                                value: 60,
                                itemStyle: {
                                    color: '#e7f563'
                                }
                            },
                        ],

                    }
                ]
            },

            particlesconfig: {
                "particles": {
                    "number": {
                        "value": 80,
                        "density": {
                            "enable": true,
                            "value_area": 800
                        }
                    },
                    "color": {
                        "value": "#ffffff"
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#000000"
                        },
                        "polygon": {
                            "nb_sides": 5
                        },
                        "image": {
                            "width": 100,
                            "height": 100
                        }
                    },
                    "opacity": {
                        "value": 0.5,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 5,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 150,
                        "color": "#ffffff",
                        "opacity": 0.4,
                        "width": 1
                    },
                    "move": {
                        "enable": true,
                        "speed": 6,
                        "direction": "none",
                        "random": false,
                        "straight": false,
                        "out_mode": "out",
                        "attract": {
                            "enable": false,
                            "rotateX": 600,
                            "rotateY": 1400
                        }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": false,
                            "mode": "repulse"
                        },
                        "onclick": {
                            "enable": false,
                            "mode": "push"
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 400,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true,
                "config_demo": {
                    "hide_card": false,
                    "background_color": "#b61924",
                    "background_image": "",
                    "background_position": "50% 50%",
                    "background_repeat": "no-repeat",
                    "background_size": "cover"
                }
            },
        }
    },
    methods: {

//JS操作cookies方法!
//写cookies
        setCookie(name, value, time) {
            //这是有设定过期时间的使用示例：
            //s20是代表20秒
            //h是指小时，如12小时则是：h12
            //d是天数，30天则：d30
            //setCookie("ggcookie","ggcookie","h24");

            var strsec = this.getsec(time);
            var exp = new Date();
            exp.setTime(exp.getTime() + strsec * 1);
            document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
        },

        getsec(str) {
            var str1 = str.substring(1, str.length) * 1;
            var str2 = str.substring(0, 1);
            if (str2 == "s") {
                return str1 * 1000;
            } else if (str2 == "h") {
                return str1 * 60 * 60 * 1000;
            } else if (str2 == "f") {
                return str1 * 60 * 1000;
            } else if (str2 == "d") {
                return str1 * 24 * 60 * 60 * 1000;
            }
        },

//读取cookies
        getCookie(name) {
            var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg)) return unescape(arr[2]);
            else return null;
        },

//删除cookies
        delCookie(name) {
            var exp = new Date();
            exp.setTime(exp.getTime() - 1);
            var cval = this.getCookie(name);
            if (cval != null) document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
        },

    }
}