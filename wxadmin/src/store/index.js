import {createStore} from 'vuex'

export default createStore({
    state: {
        menuArr: [
            {
                id: '001',
                name: '产品管理',
                icon: 'el-icon-help',
                childnode: [
                    {
                        id: '00101',
                        name: '手机',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00102',
                        name: '笔记本',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00103',
                        name: '平板',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00104',
                        name: '智能穿戴',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00105',
                        name: '智慧屏',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00106',
                        name: '智能家居',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00107',
                        name: '耳机音响',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00108',
                        name: '专属配件',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00109',
                        name: '通用产品',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00110',
                        name: '生态产品',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00111',
                        name: '增值服务',
                        orderid: '001',
                        path: '/product'
                    },
                    {
                        id: '00112',
                        name: '智能计算',
                        orderid: '001',
                        path: '/product'
                    }
                ]
            },
            {
                id: '002',
                name: '用户信息',
                icon: 'el-icon-user',
                childnode: [
                    {
                        id: '00201',
                        name: '注册用户',
                        orderid: '002',
                        path: '/user'
                    }
                ]
            },
            {
                id: '003',
                name: '订单管理',
                icon: 'el-icon-shopping-cart-2',
                childnode: [
                    {
                        id: '00301',
                        name: '全部',
                        orderid: '003',
                        path: '/order'
                    },
                ]
            },
            {
                id: '004',
                name: '系统管理',
                icon: 'el-icon-setting',
                childnode: [
                    {
                        id: '00401',
                        name: '用户信息',
                        orderid: '004',
                        path: '/systemuser'
                    },
                    {
                        id: '00402',
                        name: '角色',
                        orderid: '004',
                        path: '/roles'
                    },
                    {
                        id: '00403',
                        name: '分类管理',
                        orderid: '004',
                        path: '/category'
                    },
                ]
            },
            {
                id: '005',
                name: '界面预览',
                icon: 'el-icon-setting',
                childnode: [
                    {
                        id: '00501',
                        name: '小程序',
                        orderid: '005',
                        path: '/preview'
                    }
                ]
            }
        ]
    },
    mutations: {},
    actions: {},
})
