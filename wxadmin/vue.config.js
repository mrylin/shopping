// vue.config.js
module.exports = {
    publicPath: './', //打包后的静态文件地址
    productionSourceMap: false, //如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
    devServer: { //启动配置
        //host: 'localhost', //启动的地址,不加的话会有一个本地地址和局域网地址
        port: 8081, //启动的端口号
        /*proxy: { //跨域代理配置
            '/api': { //跨域名为/api
                target: 'http://127.0.0.1:8888', //代理的地址(服务器地址)
                ws: true, //是否允许代理websocket跨域
                changeOrigin: true, // 是否跨域
                pathRewrite: {
                    '^/api': '/api'// 替换target中的请求地址，也就是说，在请求的时候，url用'/api'代替'http://ip.taobao.com'
                }
            }
        }*/
    },

    css: {
        loaderOptions: {
            postcss: {
                // 这是rem适配的配置  注意： remUnit在这里要根据lib-flexible的规则来配制，如果您的设计稿是750px的，用75就刚刚好。
                plugins: [
                    require("postcss-adaptive")({
                        remUnit: 100,
                        autoRem: true
                    })
                ]
            }
        }
    }
};